package com.app84soft.loginsocial.utils.extension

import android.annotation.SuppressLint
import android.content.Context
import com.app84soft.loginsocial.utils.DATE_SIMPLE_FORMAT2
import com.app84soft.loginsocial.utils.EMPTY_STRING
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.abs


fun Date.timeAgo(): String {
    val SECOND_MILLIS = 1000L
    val MINUTE_MILLIS: Long = 60 * SECOND_MILLIS
    val HOUR_MILLIS: Long = 60 * MINUTE_MILLIS
    val DAY_MILLIS: Long = 24 * HOUR_MILLIS
    val MONTH_MILLIS: Long = 30 * DAY_MILLIS
    val YEAR_MILLIS: Long = 12 * MONTH_MILLIS

    var time = time
    if (time < 1000000000000L) {
        time *= 1000
    }
    val now = System.currentTimeMillis()
    if (time > now || time <= 0) {
        return "Just now"
    }
    val diff = now - time
    return when {
        diff < MINUTE_MILLIS -> {
            "Just now"
        }
        diff < 2 * MINUTE_MILLIS -> {
            "1 minute"
        }
        diff < 50 * MINUTE_MILLIS -> {
            (diff / MINUTE_MILLIS).toString() + " minutes"
        }
        diff < 90 * MINUTE_MILLIS -> {
            "1 hour"
        }
        diff < DAY_MILLIS -> {
            (diff / HOUR_MILLIS).toString() + " hours"
        }
        diff < 2 * DAY_MILLIS -> {
            "Yesterday"
        }
        diff < MONTH_MILLIS -> {
            (diff / DAY_MILLIS).toString() + " days"
        }
        diff < 2 * MONTH_MILLIS -> {
            "1 month"
        }
        diff < YEAR_MILLIS -> {
            (diff / MONTH_MILLIS).toString() + " months"
        }
        diff < 2 * YEAR_MILLIS -> {
            "1 year"
        }
        else -> {
            (diff / DAY_MILLIS).toString() + " years"
        }
    }
}

fun Date.timeAgoVi(): String {
    val SECOND_MILLIS = 1000L
    val MINUTE_MILLIS: Long = 60 * SECOND_MILLIS
    val HOUR_MILLIS: Long = 60 * MINUTE_MILLIS
    val DAY_MILLIS: Long = 24 * HOUR_MILLIS
    val MONTH_MILLIS: Long = 30 * DAY_MILLIS
    val YEAR_MILLIS: Long = 12 * MONTH_MILLIS

    var time = time
    if (time < 1000000000000L) {
        time *= 1000
    }
    val now = System.currentTimeMillis()
    if (time > now || time <= 0) {
        return "Vừa xong"
    }
    val diff = now - time
    return when {
        diff < MINUTE_MILLIS -> {
            "Vừa xong"
        }
        diff < 2 * MINUTE_MILLIS -> {
            "1 phút trước"
        }
        diff < 50 * MINUTE_MILLIS -> {
            (diff / MINUTE_MILLIS).toString() + " phút trước"
        }
        diff < 90 * MINUTE_MILLIS -> {
            "1 giờ trước"
        }
        diff < DAY_MILLIS -> {
            (diff / HOUR_MILLIS).toString() + " giờ trước"
        }
        diff < 2 * DAY_MILLIS -> {
            "Hôm qua"
        }
        diff < MONTH_MILLIS -> {
            (diff / DAY_MILLIS).toString() + " ngày trước"
        }
        diff < 2 * MONTH_MILLIS -> {
            "1 tháng trước"
        }
        diff < YEAR_MILLIS -> {
            (diff / MONTH_MILLIS).toString() + " tháng trước"
        }
        diff < 2 * YEAR_MILLIS -> {
            "1 năm trước"
        }
        else -> {
            (diff / DAY_MILLIS).toString() + " năm trước"
        }
    }
}

fun convertMillieToHMmSs(millie: Long): String {
    val seconds = millie / 1000
    val second = seconds % 60
    val minute = seconds / 60 % 60
    val hour = seconds / (60 * 60) % 24
    return if (hour > 0) {
        String.format("%02d:%02d:%02d", hour, minute, second)
    } else {
        String.format("%02d:%02d", minute, second)
    }
}


fun convertSecondToHourMinute(seconds: Long): String {
    val minute = seconds / 1000 / 60 % 60
    val hour = seconds / 1000 / (60 * 60) % 24
    return if (hour > 0 && minute <= 0) {
        String.format("%2d giờ", hour, minute)
    } else if (hour > 0) {
        String.format("%2d giờ %2d phút", hour, minute)
    } else {
        String.format("%2d phút", minute)
    }
}

fun convertSecondToHMmSs(seconds: Long): String {
    val second = seconds % 60
    val minute = seconds / 60 % 60
    val hour = seconds / (60 * 60) % 24
    return if (hour > 0) {
        String.format("%02d:%02d:%02d", hour, minute, second)
    } else {
        String.format("%02d:%02d", minute, second)
    }
}

fun convertTime(time: String?, currentFormat: String?, newFormat: String?): String? {
    if (time != null) {
        @SuppressLint("SimpleDateFormat") val formatterInput =
            SimpleDateFormat(currentFormat)
        var date: Date? = null
        try {
            date = formatterInput.parse(time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        @SuppressLint("SimpleDateFormat") val formatter =
            SimpleDateFormat(newFormat)
        return if (date == null) {
            time
        } else formatter.format(date)
    }
    return ""
}

@SuppressLint("SimpleDateFormat")
fun getCurrentDay(): String {
    val formatter: DateFormat =
        SimpleDateFormat(DATE_SIMPLE_FORMAT2)
    val currentDate = Calendar.getInstance().time
    return formatter.format(currentDate)
}
@SuppressLint("SimpleDateFormat")
fun getCurrentDayUTC(): String {
    val formatter: DateFormat = SimpleDateFormat(DATE_SIMPLE_FORMAT2)
    formatter.timeZone = TimeZone.getTimeZone("UTC")
    val currentDate = Calendar.getInstance().time
    return formatter.format(currentDate)
}

fun convertTimeSaveTimeZone(time: String?, currentFormat: String?, newFormat: String?): String? {
    if (time != null) {
        @SuppressLint("SimpleDateFormat") val formatterInput =
            SimpleDateFormat(currentFormat)
        formatterInput.timeZone = TimeZone.getTimeZone("UTC")
        var date: Date? = null
        try {
            date = formatterInput.parse(time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        @SuppressLint("SimpleDateFormat") val formatter =
            SimpleDateFormat(newFormat)
        formatter.timeZone = TimeZone.getTimeZone("UTC")
        return if (date == null) {
            time
        } else formatter.format(date)
    }
    return ""
}

fun convertStringTimeToDate(time: String, currentFormat: String?): Date? {
    @SuppressLint("SimpleDateFormat") val formatterInput =
        SimpleDateFormat(currentFormat)
    try {
        return formatterInput.parse(time)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return null
}

@SuppressLint("SimpleDateFormat")
fun convertLongTimeToString(time: Long, newFormat: String): String? {
    val df = SimpleDateFormat(newFormat)
    return df.format(time)
}

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@SuppressLint("SimpleDateFormat")
fun getCountOfDays(startDate: Date, endDate: Date): Float {
    val difference = abs(startDate.time - endDate.time)
    return (difference.toFloat() / (24 * 60 * 60 * 1000))
}

@SuppressLint("SimpleDateFormat")
fun convertToTimeAgo(
    time: String?,
    currentFormat: String?,
    newFormat: String?,
    context: Context
): String {
    if (time != null) {
        val formatterInput = SimpleDateFormat(currentFormat)
        formatterInput.timeZone = TimeZone.getTimeZone("UTC")
        var date: Date? = null
        try {
            date = formatterInput.parse(time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val formatter =
            SimpleDateFormat(newFormat)
        formatter.timeZone = TimeZone.getDefault()
        return if (date == null) {
            time
        } else {
            val sdf = SimpleDateFormat(newFormat)
            try {
                val d = sdf.parse(sdf.format(date))
                d.timeAgoVi()
            } catch (ex: ParseException) {

            }
            return EMPTY_STRING
        }
    } else {
        return EMPTY_STRING
    }
}
