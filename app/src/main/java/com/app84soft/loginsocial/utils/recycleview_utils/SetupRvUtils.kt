package com.app84soft.loginsocial.utils

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.*
import com.app84soft.loginsocial.utils.recycleview_utils.CenterLayoutManager
import com.app84soft.loginsocial.utils.recycleview_utils.GridSpacingItemDecoration
import com.app84soft.loginsocial.utils.recycleview_utils.SpeedyLinearLayoutManager


fun setupLinearLayoutRecyclerView(
    context: Context?,
    recyclerView: RecyclerView
) {
    val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
    recyclerView.layoutManager = mLayoutManager
    recyclerView.itemAnimator = DefaultItemAnimator()
    recyclerView.setHasFixedSize(true)
    recyclerView.isNestedScrollingEnabled = false
}

fun setupSpeedyLinearLayoutRecyclerView(
    context: Context?,
    recyclerView: RecyclerView
) {
    val mLayoutManager: RecyclerView.LayoutManager =
        SpeedyLinearLayoutManager(
            context,
            SpeedyLinearLayoutManager.VERTICAL,
            false
        )
    recyclerView.layoutManager = mLayoutManager
    recyclerView.itemAnimator = DefaultItemAnimator()
    recyclerView.setHasFixedSize(true)
    recyclerView.isNestedScrollingEnabled = false
}

fun setupLinearLayoutHorizontalRecyclerView(
    context: Context?,
    recyclerView: RecyclerView
) {
    // setLayoutManager
    val mLayoutManager: RecyclerView.LayoutManager =
        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    recyclerView.layoutManager = mLayoutManager
}

fun setupLinearLayoutRecyclerView1(
    context: Context?,
    recyclerView: RecyclerView
) {
    // setLayoutManager
    val mLayoutManager =
        CenterLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
    recyclerView.layoutManager = mLayoutManager
    recyclerView.itemAnimator = DefaultItemAnimator()
    recyclerView.setHasFixedSize(true)
    recyclerView.setNestedScrollingEnabled(false);
}

fun setupLinearLayoutWithDividerRecyclerView(
    context: Context?,
    recyclerView: RecyclerView
) {
    // setLayoutManager
    val llm = LinearLayoutManager(context)
    llm.orientation = LinearLayoutManager.VERTICAL
    val DividerItemDecoration = DividerItemDecoration(
        recyclerView.context,
        llm.orientation
    )
    recyclerView.addItemDecoration(DividerItemDecoration)
    recyclerView.itemAnimator = DefaultItemAnimator()
    recyclerView.setHasFixedSize(true)
    recyclerView.isNestedScrollingEnabled = false
    recyclerView.layoutManager = llm
}

fun setupStaggeredGridRecyclerView(recyclerView: RecyclerView, column: Int) {
    val mLayoutManager =
        StaggeredGridLayoutManager(column, StaggeredGridLayoutManager.VERTICAL)
    recyclerView.layoutManager = mLayoutManager
    recyclerView.setItemViewCacheSize(20)
    recyclerView.isDrawingCacheEnabled = true
    recyclerView.drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
}

fun setupGridLayoutRecyclerView(
    context: Context?,
    recyclerView: RecyclerView,
    column: Int,
    spacingInPixels: Int
) {
    val mLayoutManager: RecyclerView.LayoutManager = GridLayoutManager(context, column)
    recyclerView.layoutManager = mLayoutManager
    recyclerView.itemAnimator = DefaultItemAnimator()
    recyclerView.setHasFixedSize(true)
    recyclerView.addItemDecoration(
        GridSpacingItemDecoration(
            2,
            spacingInPixels,
            true,
            0
        )
    )
}

fun setupGridLayoutRecyclerViewHorizontalWithRatio(
    context: Context?,
    recyclerView: RecyclerView,
    column: Int, screenRatio: Float
) {
    recyclerView.layoutManager =
        object : GridLayoutManager(context, column, HORIZONTAL, false) {
            override fun checkLayoutParams(lp: RecyclerView.LayoutParams): Boolean {
                lp.width = (width * screenRatio).toInt()
                return true
            }
        }
}

fun setupGridLayoutRecyclerViewHorizontal(
    context: Context?,
    recyclerView: RecyclerView,
    column: Int
) {
    recyclerView.layoutManager = GridLayoutManager(context, column, GridLayoutManager.HORIZONTAL,false)
}