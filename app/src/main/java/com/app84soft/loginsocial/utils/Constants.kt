package com.app84soft.loginsocial.utils

import com.app84soft.loginsocial.injection.ApplicationComponent

//User
const val PREF_USER_DATA = "PREF_USER"
const val PREF_ACCESS_TOKEN = "PREF_ACCESS_TOKEN"
const val PREF_ACCOUNT: String = "PREF_ACCOUNT"
const val PREF_PASSWORD: String = "PREF_PASS"
const val PREF_LOGIN_TYPE: String = "PREF_LOGIN_TYPE"
const val EXTRA_PHONE_NUMBER: String = "EXTRA_PHONE_NUMBER"
const val EXTRA_OTP: String = "EXTRA_OTP"
const val EXTRA_FROM_WELCOME: String = "EXTRA_FROM_WELCOME"
const val REGEX_PASS_WORD: String= "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$"
//Date format
const val FORMAT_DATE: String = "HH:mm dd/MM/yyyy"
const val DATE_SIMPLE_FORMAT: String = "dd/MM/yyyy"
const val DATE_SIMPLE_FORMAT2: String = "yyyy-MM-dd"
const val DATE_TIME_FORMAT: String = "HH:mm dd/MM/yyyy"
const val DATE_TIME_FORMAT2: String = "dd/MM/yyyy HH:mm"
const val DATE_FULL_FORMAT: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"
const val DATE_FULL_FORMAT2: String = "yyyy-MM-dd'T'HH:mm:ss.SSS"
const val DATE_FORMAT: String = "yyyy/MM/dd HH:mm:ss"
const val TIME_FORMAT: String = "HH:mm"

var myapp: ApplicationComponent? = null
const val DEFAULT_LANGUAGE = "vi"
const val EMPTY_STRING = ""
const val DEFAULT_INT_VALUE = 0
const val DEFAULT_LONG_VALUE: Long = 0
const val DEFAULT_FLOAT_VALUE: Float = 0f
const val ZERO_POSITION = 0
const val DEFAULT_FIRST_PAGE = 0
const val ANDROID = "android"
const val DIMEN = "dimen"
const val STATUS_BAR_HEIGHT = "status_bar_height"
const val NAVIGATION_BAR_HEIGHT = "navigation_bar_height"
const val ALLOWED_CHARACTERS =
    "0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM"
const val URL_MECHANISM = "http://65.108.149.39:8082/law.html"
const val DOUBLE_PRESS_INTERVAL: Long = 1000

const val REQUEST_CODE = 100

const val TYPE_CATEGORY = 1
const val TYPE_TUTORIAL = 2
const val TYPE_MISSION = 3
const val TYPE_SELLING = 4
const val TYPE_NEW_PRODUCT = 5
const val TYPE_FASHION_TRENDS = 6
const val TYPE_ANTI_EVIDENCE = 7
const val TYPE_ENDOW = 8
const val TYPE_GRATEFUL = 9
const val TYPE_BRAND_OFFER = 10
const val TYPE_EASY_OPTIONS_FOR_SALE = 11
const val TYPE_HIGH_PROFIT_SELL_GOOD_BILLION = 12
const val TYPE_HINT_TODAY = 96
const val TYPE_TODAY_SUGGESTION = 13
const val TYPE_TITLE_HINT_TODAY = 69
const val TYPE_DEFAULT = 6969
const val DEFAULT_SIZE = 0

const val PREF_SEARCH_HISTORY: String = "PREF_SEARCH_HISTORY"

