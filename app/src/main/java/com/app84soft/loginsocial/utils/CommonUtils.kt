package com.app84soft.loginsocial.utils

import android.animation.Animator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.media.ExifInterface
import android.net.http.SslError
import android.os.Build
import android.provider.Settings
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.util.Patterns
import android.util.Size
import android.util.TypedValue
import android.view.*
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.Transformation
import android.view.inputmethod.InputMethodManager
import android.webkit.SslErrorHandler
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app84soft.loginsocial.BuildConfig
import com.app84soft.loginsocial.R
import com.bumptech.glide.GenericTransitionOptions
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import java.io.*
import java.security.MessageDigest
import java.security.spec.KeySpec
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec
import kotlin.math.ln
import kotlin.math.pow


object CommonUtils {

    private fun getActivityRoot(window: Window): View {
        return (window.decorView.rootView.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(
            0
        )
    }

    fun isValidEmail(target: CharSequence): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

    fun getDPtoPX(context: Context, dp: Float): Int {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, dp,
            context.resources.displayMetrics
        ).toInt()
    }

    private const val KEYBOARD_VISIBLE_THRESHOLD_DP = 100f


    interface KeyboardVisibilityEventListener {
        fun onVisibilityChanged(isOpen: Boolean)
    }

    fun printKeyHash(context: Context) {
        if (BuildConfig.DEBUG) {
            try {
                val info: PackageInfo = context.packageManager.getPackageInfo(
                    context.packageName,
                    PackageManager.GET_SIGNATURES
                )
                for (signature in info.signatures) {
                    val md: MessageDigest = MessageDigest.getInstance("SHA")
                    md.update(signature.toByteArray())
                    Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
                }
            } catch (e: Exception) {
            }
        }
    }

    fun setEventListener(
        activity: Activity,
        listener: KeyboardVisibilityEventListener?,
        window: Window
    ) {
        if (listener == null) {
            return
        }

        val activityRoot = getActivityRoot(window) ?: return
        activityRoot.viewTreeObserver
            .addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {

                private val r = Rect()

                private val visibleThreshold = Math.round(
                    getDPtoPX(
                        activity,
                        KEYBOARD_VISIBLE_THRESHOLD_DP
                    ).toFloat()
                )

                private var wasOpened = false

                override fun onGlobalLayout() {
                    activityRoot.getWindowVisibleDisplayFrame(r)

                    val heightDiff = activityRoot.rootView.height - r.height()

                    val isOpen = heightDiff > visibleThreshold
                    if (isOpen == wasOpened) {
                        return
                    }
                    wasOpened = isOpen
                    listener.onVisibilityChanged(isOpen)
                }
            })
    }


    fun setImageRadiusFormUrl(imageView: ImageView, url: String?, radius: Int) {
        val context = imageView.context
        Glide.with(context)
            .load(url)
            .transition(GenericTransitionOptions.with(R.anim.fade_in))
            .transform(CenterCrop(), RoundedCorners(radius))
            .error(R.drawable.ic_picture_nodata)
            .into(imageView)
    }

    fun setImageFormUrl(imageView: ImageView, url: String?) {
        val context = imageView.context
        Glide.with(context)
            .load(url)
            .transition(GenericTransitionOptions.with(R.anim.fade_in))
            .error(R.drawable.ic_picture_nodata)
            .into(imageView)
    }

    fun setImageWithoutAnimate(imageView: ImageView, url: String?, radius: Int) {
        val context = imageView.context
        Glide.with(context)
            .load(url)
            .transform(CenterCrop(), RoundedCorners(radius))
            .error(R.drawable.ic_picture_nodata)
            .into(imageView)
    }

    fun setImageAvatarFormUrl(imageView: ImageView, url: String?) {
        val context = imageView.context
        Glide.with(context)
            .load(url)
            .transition(GenericTransitionOptions.with(R.anim.fade_in))
            .error(R.drawable.ic_avata_default)
            .into(imageView)
    }

    fun getImageDrawable(context: Context, id: Int): Drawable {
        return AppCompatResources.getDrawable(context, id)!!
    }

    private fun getTypeDate(c: Calendar): String {
        val sdf2 = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        return sdf2.format(c.time)
    }

    @SuppressLint("HardwareIds")
    fun getID(context: Context): String? {
        return Settings.Secure.getString(
            context.contentResolver,
            Settings.Secure.ANDROID_ID
        )
    }

    fun versionName(context: Context): String? {
        try {
            val pInfo: PackageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            return pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        return null
    }

    fun convertMillieToHMmSs(millie: Long): String? {
        val seconds = millie / 1000
        val second = seconds % 60
        val minute = seconds / 60 % 60
        val hour = seconds / (60 * 60) % 24
        val result = ""
        return if (hour > 0) {
            String.format("%02d:%02d:%02d", hour, minute, second)
        } else {
            String.format("%02d:%02d", minute, second)
        }
    }

    fun convertDpToPx(context: Context, dp: Int): Float {
        val r = context.resources
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dp.toFloat(),
            r.displayMetrics
        )
    }

    fun setDefaultAvatar(username: String, tv: TextView) {
        try {
            var nameSet = EMPTY_STRING
            val name = username.split(" ").toTypedArray()
            if (name.isNotEmpty()) {
                for (s in name) {
                    nameSet += s[ZERO_POSITION]
                }
            }
            if (nameSet.length > 1) {
                nameSet = nameSet.substring(nameSet.length - 2)
            }
            tv.text = nameSet
        } catch (e: Exception) {
            e.printStackTrace()
            tv.text = "?"
        }
    }

    fun startAlphaAnimation(v: View, duration: Long, visibility: Int) {
        val alphaAnimation =
            if (visibility == View.VISIBLE) AlphaAnimation(0f, 1f) else AlphaAnimation(1f, 0f)
        alphaAnimation.duration = duration
        alphaAnimation.fillAfter = true
        v.startAnimation(alphaAnimation)
    }

    fun EditText.showKeyboard() {
        post {
            requestFocus()
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
        }
    }


    fun getStatusBarHeight(context: Context): Int {
        var result = DEFAULT_INT_VALUE
        val resourceId =
            context.resources.getIdentifier(STATUS_BAR_HEIGHT, DIMEN, ANDROID)
        if (resourceId > DEFAULT_INT_VALUE) {
            result = context.resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    fun getBottomNavigationBarHeight(context: Context): Int {
        val result = DEFAULT_INT_VALUE
        val resourceId: Int = context.resources.getIdentifier(
            NAVIGATION_BAR_HEIGHT,
            DIMEN,
            ANDROID
        )
        return if (resourceId > DEFAULT_INT_VALUE) {
            context.resources.getDimensionPixelSize(resourceId)
        } else result
    }

    fun pxFromDp(context: Context, dp: Float): Int {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, dp,
            context.resources.displayMetrics
        ).toInt()
    }

    fun formatMoney(number: String?): String? {
        val formatter = DecimalFormat("###,###,###,###.###")
        return try {
            val d: Double =
                java.lang.Double.valueOf(number?.let { correctNumber(it) }?.trim() ?: EMPTY_STRING)
            formatter.format(d)
//                .replace(",", ".")
        } catch (e: java.lang.Exception) {
            number
        }
    }

    fun withSuffix(count: Long): String {
        if (count < 1000) return "" + count
        val exp = (ln(count.toDouble()) / ln(1000.0)).toInt()
        return String.format(
            "%.1f%c",
            count / 1000.0.pow(exp.toDouble()),
            "KMGTPE"[exp - 1]
        )
    }

    private fun correctNumber(number: String): String {
        return number.replace("[\\D]".toRegex(), EMPTY_STRING)
    }

    fun getRandomString(): String {
        val random = Random()
        val sb = StringBuilder(20)
        for (i in 0 until 20)
            sb.append(ALLOWED_CHARACTERS[random.nextInt(ALLOWED_CHARACTERS.length)])
        return sb.toString() + System.currentTimeMillis()
    }

    fun Activity.hideSoftKeyboard() {
        currentFocus?.let {
            val inputMethodManager = ContextCompat.getSystemService(
                this,
                InputMethodManager::class.java
            )
            inputMethodManager?.hideSoftInputFromWindow(it.windowToken, 0)
        }
    }

    fun setupUI(view: View?, activity: Activity?) {
        if (view !is EditText) {
            view?.setOnTouchListener { v, _ ->
                v?.performClick()
                activity?.hideSoftKeyboard()
                false
            }
        }

        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                val innerView = view.getChildAt(i)
                setupUI(innerView, activity)
            }
        }
    }

    fun getRatioScreen(activity: Activity): Double {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val metrics: WindowMetrics = activity.windowManager.currentWindowMetrics
            val windowInsets = metrics.windowInsets
            val insets: Insets = windowInsets.getInsetsIgnoringVisibility(
                WindowInsets.Type.navigationBars()
                        or WindowInsets.Type.displayCutout()
            )

            val insetsWidth: Int = insets.right + insets.left
            val insetsHeight: Int = insets.top + insets.bottom

            // Legacy size that Display#getSize reports
            val bounds: Rect = metrics.bounds
            val legacySize = Size(
                bounds.width() - insetsWidth,
                bounds.height() - insetsHeight
            )
            return legacySize.width.toDouble() / legacySize.height
        } else {
            val display = activity.windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            val width = size.x
            val height = size.y
            return width.toDouble() / height
        }
    }

    fun getFullWidth(activity: Activity): Double {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val metrics: WindowMetrics = activity.windowManager.currentWindowMetrics
            val windowInsets = metrics.windowInsets
            val insets: Insets = windowInsets.getInsetsIgnoringVisibility(
                WindowInsets.Type.navigationBars()
                        or WindowInsets.Type.displayCutout()
            )

            val insetsWidth: Int = insets.right + insets.left
            val insetsHeight: Int = insets.top + insets.bottom

            // Legacy size that Display#getSize reports
            val bounds: Rect = metrics.bounds
            val legacySize = Size(
                bounds.width() - insetsWidth,
                bounds.height() - insetsHeight
            )
            return legacySize.width.toDouble()
        } else {
            val display = activity.windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            return size.x.toDouble()
        }
    }

    fun getFullHeight(activity: Activity): Double {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val metrics: WindowMetrics = activity.windowManager.currentWindowMetrics
            val windowInsets = metrics.windowInsets
            val insets: Insets = windowInsets.getInsetsIgnoringVisibility(
                WindowInsets.Type.navigationBars()
                        or WindowInsets.Type.displayCutout()
            )

            val insetsWidth: Int = insets.right + insets.left
            val insetsHeight: Int = insets.top + insets.bottom

            // Legacy size that Display#getSize reports
            val bounds: Rect = metrics.bounds
            val legacySize = Size(
                bounds.width() - insetsWidth,
                bounds.height() - insetsHeight
            )
            return legacySize.height.toDouble()
        } else {
            val display = activity.windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            return size.y.toDouble()
        }
    }

    fun formatNumber(number: Double): String? {
        val formatter = NumberFormat.getNumberInstance(Locale.US) as DecimalFormat
        formatter.applyPattern("###,###.#")
        return try {
            formatter.format(number)
        } catch (e: Exception) {
            number.toString()
        }
    }

    fun createRotatedFile(filePath: String, activity: Activity): File {
        val ei = ExifInterface(filePath)
        val orientation: Int = ei.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )
        val options = BitmapFactory.Options()
        options.inSampleSize = 2
        val bitmap = BitmapFactory.decodeFile(filePath, options)

        val rotatedBitmap = when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(bitmap, 90f, activity)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(bitmap, 180f, activity)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(bitmap, 270f, activity)
            else -> rotateImage(bitmap, 0f, activity)
        }
        return rotatedBitmap.persistImage(getRandomString(), activity)
    }

    private fun rotateImage(source: Bitmap, angle: Float, activity: Activity): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        val bitmap = Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
        val bitmapDrawable = BitmapDrawable(activity.resources, bitmap)
        val width: Int = bitmapDrawable.intrinsicWidth
        val height: Int = bitmapDrawable.intrinsicHeight
        val ratioImage = width.toDouble() / height
        val b = bitmapDrawable.bitmap

        var widthResize = width
        var heightResize = height
        if (ratioImage > 1) { // ảnh ngang
            if (width > 1920) {
                widthResize = 1920
                heightResize = (widthResize / ratioImage).toInt()
            }
        } else { // ảnh dọc
            if (height > 1920) {
                heightResize = 1920
                widthResize = (heightResize * ratioImage).toInt()
            }
        }
        return Bitmap.createScaledBitmap(b, widthResize, heightResize, false)
    }

    private fun Bitmap?.persistImage(name: String, context: Context): File {
        val filesDir: File = context.filesDir
        val imageFile = File(filesDir, "$name.png")
        val os: OutputStream
        try {
            os = FileOutputStream(imageFile)
            this?.compress(Bitmap.CompressFormat.PNG, 100, os)
            os.flush()
            os.close()
        } catch (e: Exception) {
            Log.e(javaClass.simpleName, "Error writing bitmap", e)
        }
        return imageFile
    }

    fun runAnimationFromRight(recyclerView: RecyclerView?) {
        if (recyclerView != null) {
            val context = recyclerView.context
            val controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_from_rignt)
            recyclerView.layoutAnimation = controller
            recyclerView.adapter?.notifyDataSetChanged()
            recyclerView.scheduleLayoutAnimation()
        }
    }

    fun showAlertErrorSSl(
        context: Context?,
        handler: SslErrorHandler?,
        error: SslError?
    ) {
        var message = "SSL Certificate error."
        when (error?.primaryError) {
            SslError.SSL_UNTRUSTED -> message = "The certificate authority is not trusted."
            SslError.SSL_EXPIRED -> message = "The certificate has expired."
            SslError.SSL_IDMISMATCH -> message = "The certificate Hostname mismatch."
            SslError.SSL_NOTYETVALID -> message = "The certificate is not yet valid."
        }
        message += " Do you want to continue anyway?"
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setTitle("SSL Certificate Error")
        builder.setMessage(message)
        builder.setPositiveButton("continue") { _, _ -> handler?.proceed() }
        builder.setNegativeButton("cancel") { _, _ -> handler?.cancel() }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    fun showCustomUI(activity: Activity) {
        activity.window.statusBarColor = 0x00000000
        activity.window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
    }

    fun showHideAnimate(isShow: Boolean, view: View) {
        val alpha: Float

        val visible = if (isShow) {
            alpha = 1.0f
            View.VISIBLE
        } else {
            alpha = 0.0f
            View.GONE
        }

        view.animate().alpha(alpha).setListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator?) {
                if (isShow) {
                    view.alpha = 0.0f
                    view.visibility = View.VISIBLE
                }
            }

            override fun onAnimationEnd(animation: Animator?) {
                view.visibility = visible
            }

            override fun onAnimationCancel(animation: Animator?) {

            }

            override fun onAnimationRepeat(animation: Animator?) {

            }
        }).duration = 300

    }

    fun expand(v: View, time: Int) {
        val matchParentMeasureSpec =
            View.MeasureSpec.makeMeasureSpec((v.parent as View).width, View.MeasureSpec.EXACTLY)
        val wrapContentMeasureSpec =
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec)
        val targetHeight = v.measuredHeight

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.layoutParams.height = 0
        v.visibility = View.VISIBLE
        val a: Animation = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                v.layoutParams.height =
                    if (interpolatedTime == 1f) ConstraintLayout.LayoutParams.WRAP_CONTENT else (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // Expansion speed of 1dp/ms
        a.duration = (targetHeight / v.context.resources.displayMetrics.density).toLong() * time
        v.startAnimation(a)
    }

    fun collapse(v: View, time: Int) {
        val initialHeight = v.measuredHeight
        val a: Animation = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height =
                        initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // Collapse speed of 1dp/ms
        a.duration = (initialHeight / v.context.resources.displayMetrics.density).toLong() * time
        v.startAnimation(a)
    }

    val CRYPTO_SALT: String = "84soft@123"
    val CRYPTO_MASTER_KEY = "7xv89d8fds9fyvsdndfs7ssdvd68sdnd"
    val CRYPTO_IV = "2c43c2v42b4gy4y2"
    fun encrypt(strToEncrypt: String?): String {
        try {
            val iv = CRYPTO_IV.toByteArray()
            val ivspec = IvParameterSpec(iv)
            val factory: SecretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256")
            val spec: KeySpec =
                PBEKeySpec(CRYPTO_MASTER_KEY.toCharArray(), CRYPTO_SALT.toByteArray(), 2145, 256)
            val tmp: SecretKey = factory.generateSecret(spec)
            val secretKey = SecretKeySpec(tmp.encoded, "AES")
            val cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING")
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivspec)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                return java.util.Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt?.toByteArray()))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return strToEncrypt ?: ""
    }
}

