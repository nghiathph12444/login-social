package com.app84soft.loginsocial.data;

import com.app84soft.loginsocial.data.prefs.PreferencesHelper;
import com.app84soft.loginsocial.data.realm.RealmHelper;

public interface DataManager extends PreferencesHelper, RealmHelper {

}
