package com.app84soft.loginsocial.ui.main.home

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.wifi.WifiManager
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app84soft.loginsocial.R
import com.app84soft.loginsocial.base.BaseFragment
import com.app84soft.loginsocial.databinding.FragmentHomeBinding
import com.app84soft.loginsocial.utils.CommonUtils
import com.app84soft.loginsocial.utils.DEFAULT_INT_VALUE
import com.app84soft.loginsocial.utils.EMPTY_STRING
import com.app84soft.loginsocial.utils.REQUEST_CODE
import com.app84soft.loginsocial.utils.recycleview_utils.EndlessRecyclerViewScrollListener
import com.app84soft.loginsocial.view_model.ViewModelFactory

class HomeFragment : BaseFragment<HomeFragmentViewModel, FragmentHomeBinding>() {
    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun getContentLayout(): Int {
        return R.layout.fragment_home
    }

    override fun initViewModel() {
        viewModel =
            ViewModelProvider(
                this,
                ViewModelFactory(binding.root.context)
            )[HomeFragmentViewModel::class.java]
    }

    override fun initView() {
        paddingStatusBar(binding.layoutParent)
    }

    override fun initListener() {


    }


    override fun observerLiveData() {

    }

}