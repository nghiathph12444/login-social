package com.app84soft.loginsocial.ui.otp

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.app84soft.loginsocial.data.DataManager
import com.app84soft.loginsocial.model.request.ForgotPasswordRequest
import com.app84soft.loginsocial.model.request.OTP
import com.app84soft.loginsocial.network.Api
import com.app84soft.loginsocial.view_model.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


@SuppressLint("CheckResult")
class OtpViewModel(private val context: Context) : BaseViewModel() {
    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var api: Api


     var otpResponse: MutableLiveData<Boolean> = MutableLiveData()
    fun getOtp(request: OTP) {
        api.getOTP(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrievePostListStart() }
            .doOnTerminate { onRetrievePostListFinish() }
            .subscribe({
                otpResponse.postValue(true)
            },
                {
                    handleApiError(it)
                })
    }

    var changeForgotPassResponse: MutableLiveData<String> = MutableLiveData()
    fun changeForgotPass(forgotPasswordRequest: ForgotPasswordRequest){
        api.forgotPassword(forgotPasswordRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrievePostListStart() }
            .doOnTerminate { onRetrievePostListFinish() }
            .subscribe({
                changeForgotPassResponse.postValue(it?.message.toString())
            },
                {
                    handleApiError(it)
                })
    }

}