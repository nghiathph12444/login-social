package com.app84soft.loginsocial.ui.splash

import android.annotation.SuppressLint
import android.content.Context
import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import com.app84soft.loginsocial.R
import com.app84soft.loginsocial.base.AccountUtil
import com.app84soft.loginsocial.data.DataManager
import com.app84soft.loginsocial.model.constants.TypeAutoLogin
import com.app84soft.loginsocial.model.constants.TypeLogin
import com.app84soft.loginsocial.model.request.LoginRequest
import com.app84soft.loginsocial.model.request.SocialRequest
import com.app84soft.loginsocial.network.Api
import com.app84soft.loginsocial.utils.CommonUtils.encrypt
import com.app84soft.loginsocial.view_model.BaseViewModel
import com.facebook.AccessToken
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


@SuppressLint("CheckResult")
class SplashViewModel(val context: Context) : BaseViewModel() {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var api: Api

    var autoLoginResponse: MutableLiveData<TypeAutoLogin> = MutableLiveData()

    fun checkLogin() {
        when (AccountUtil().getInstance(context)?.getTypeLogin()) {
            TypeLogin.MANUAL.value -> {
                val username = AccountUtil().getInstance(context)?.getAccount()
                val password = AccountUtil().getInstance(context)?.getPassword()
                val deviceKey = com.app84soft.loginsocial.utils.CommonUtils.getID(context?.applicationContext)
                if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
                    loginManual(LoginRequest(username, encrypt(password), deviceKey),password)
                } else {
                    autoLoginResponse.postValue(TypeAutoLogin.TO_LOGIN)
                }
            }
            TypeLogin.GOOGLE.value -> {
                refreshIdTokenGoogle(context)
            }
            TypeLogin.FACEBOOK.value -> {
                val accessToken = AccessToken.getCurrentAccessToken()
                if (accessToken?.token != null) {
                    loginFacebook(accessToken.token)
                } else {
                    autoLoginResponse.postValue(TypeAutoLogin.TO_LOGIN)
                }
            }
            else -> {
                autoLoginResponse.postValue(TypeAutoLogin.TO_WELCOME)
                AccountUtil().getInstance(context)?.logout(context)
            }
        }
    }

    private fun loginManual(request: LoginRequest, password:String?) {
        api.loginManual(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    AccountUtil().getInstance(context)?.saveLoginInfo(
                        it?.data,
                        request.username,
                        password,
                        TypeLogin.MANUAL
                    )
                    autoLoginResponse.postValue(TypeAutoLogin.TO_MAIN)

                },
                {
                    autoLoginResponse.postValue(TypeAutoLogin.TO_LOGIN)
                }
            )
    }

    private fun loginFacebook(tokenFacebook: String) {
        api.loginFacebook(SocialRequest(tokenFacebook))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    AccountUtil().getInstance(context)
                        ?.saveLoginInfo(it?.data, null, null, TypeLogin.FACEBOOK)
                    autoLoginResponse.postValue(TypeAutoLogin.TO_MAIN)
                },
                {
                    autoLoginResponse.postValue(TypeAutoLogin.TO_LOGIN)
                }
            )
    }

    private fun loginGoogle(tokenGoogle: String) {
        api.loginGoogle(SocialRequest(tokenGoogle))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    AccountUtil().getInstance(context)
                        ?.saveLoginInfo(it?.data, null, null, TypeLogin.GOOGLE)
                    autoLoginResponse.postValue(TypeAutoLogin.TO_MAIN)
                },
                {
                    autoLoginResponse.postValue(TypeAutoLogin.TO_LOGIN)
                }
            )
    }

    private fun refreshIdTokenGoogle(context: Context) {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(context.getString(R.string.server_client_id))
            .requestEmail()
            .build()
        val mGoogleApiClient = GoogleSignIn.getClient(context, gso)
        val opr = mGoogleApiClient.silentSignIn()
        if (opr.isSuccessful) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            val result = opr.result
            // handleSignInResult(result);  // result.getSignInAccount().getIdToken(), etc.
            loginGoogle(result?.idToken.toString().trim())
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            GoogleSignIn.getClient(context, gso).silentSignIn().addOnSuccessListener {
//                Toast.makeText(context, "TOKEN_GOOGLE_SUCCESS", Toast.LENGTH_SHORT).show()
                loginGoogle(it?.idToken.toString().trim())
            }.addOnFailureListener {
                autoLoginResponse.postValue(TypeAutoLogin.TO_LOGIN)
            }.addOnCanceledListener {
                autoLoginResponse.postValue(TypeAutoLogin.TO_LOGIN)
            }
        }
    }

}