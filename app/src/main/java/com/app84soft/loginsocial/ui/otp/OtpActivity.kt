package com.app84soft.loginsocial.ui.otp

import android.content.DialogInterface
import android.content.Intent
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.text.HtmlCompat
import androidx.lifecycle.ViewModelProvider
import com.app84soft.loginsocial.R
import com.app84soft.loginsocial.base.AccountUtil
import com.app84soft.loginsocial.base.BaseActivity
import com.app84soft.loginsocial.databinding.ActivityOtpBinding
import com.app84soft.loginsocial.model.request.LogoutRequest
import com.app84soft.loginsocial.model.request.VerifyRequest
import com.app84soft.loginsocial.ui.login.LoginActivity
import com.app84soft.loginsocial.ui.main.MainActivity

import com.app84soft.loginsocial.utils.*
import com.app84soft.loginsocial.view_model.ViewModelFactory
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import java.util.concurrent.TimeUnit

class OtpActivity : BaseActivity<OtpViewModel, ActivityOtpBinding>() {

    private lateinit var phone: String
    private var mVerificationId = EMPTY_STRING
    private lateinit var auth: FirebaseAuth
    lateinit var timer: CountDownTimer
    private var isOtpValidated = false
    private var backPressedTime: Long = 0
    private val TIME_OUT = 60L

    override fun getContentLayout(): Int {
        return R.layout.activity_otp
    }

    override fun initViewModel() {
        viewModel =
            ViewModelProvider(this, ViewModelFactory(this))[OtpViewModel::class.java]
    }

    override fun initView() {
        setColorForStatusBar(R.color.colorWhite)
        setLightIconStatusBar(false)
    }

    override fun initListener() {
        phone = intent.getStringExtra(EXTRA_PHONE_NUMBER) ?: EMPTY_STRING
        binding.tvContent.text = getString(R.string.str_otp_content, phone)

        auth = Firebase.auth
        auth.setLanguageCode(DEFAULT_LANGUAGE)

        binding.tvTimeLeft.isClickable = false
        binding.tvTimeLeft.setOnClickListener {
            binding.tvTimeLeft.isClickable = false
            requestOTP(phone)
            timer.start()
        }
        requestOTP(phone)

        timer = object : CountDownTimer(TIME_OUT * DOUBLE_PRESS_INTERVAL, DOUBLE_PRESS_INTERVAL) {
            override fun onTick(millisUntilFinished: Long) {
                binding.tvTimeLeft.text =
                    getString(R.string.str_time_left, millisUntilFinished / DOUBLE_PRESS_INTERVAL)
            }

            override fun onFinish() {
                binding.tvTimeLeft.isClickable = true
                binding.tvTimeLeft.text = HtmlCompat.fromHtml(
                    binding.root.context.getString(R.string.str_request_otp),
                    HtmlCompat.FROM_HTML_MODE_LEGACY
                )
            }
        }
        timer.start()

        binding.edtOtp.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                val lengthOtp = 6
                if (TextUtils.isEmpty(s.toString().trim())) {
                    binding.textInputOtp.error = getString(R.string.str_empty_otp)
                    isOtpValidated = false
                } else {
                    if (s.toString().trim().length != lengthOtp) {
                        binding.textInputOtp.error = getString(R.string.str_error_otp)
                        isOtpValidated = false
                    } else {
                        binding.textInputOtp.error = null
                        isOtpValidated = true
                    }
                }
            }
        })

        binding.tvConfirm.setOnClickListener {
            if (!isDoubleClick()) {
                if (isOtpValidated) {
                    confirmOTPFirebase(binding.edtOtp.text.toString().trim())
                }
            }
        }

        binding.tvLogout.setOnClickListener {
            if (!isDoubleClick()) {
                val alertDialogBuilder = AlertDialog.Builder(binding.root.context)
                alertDialogBuilder.setIcon(R.mipmap.ic_launcher)
                alertDialogBuilder.setTitle(getString(R.string.app_name))
                alertDialogBuilder.setMessage(getString(R.string.str_log_out))
                alertDialogBuilder.setPositiveButton(getString(R.string.str_yes)) { arg0: DialogInterface, _: Int ->
                    arg0.dismiss()

                }

                alertDialogBuilder.setNegativeButton(
                    getString(R.string.str_no)
                ) { dialog: DialogInterface, _: Int -> dialog.dismiss() }

                val alertDialog = alertDialogBuilder.create()
                alertDialog.show()
            }
        }
    }

    override fun observerLiveData() {
        viewModel.apply {

        }
    }

    private fun requestOTP(phone: String) {
        val options = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber(phone.replaceFirst("0", "+84"))       // Phone number to verify
            .setTimeout(TIME_OUT, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this)                 // Activity (for callback binding)
            .setCallbacks(object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                    Log.d("OTP_FIREBASE", "onVerificationCompleted:$credential")
                }

                override fun onVerificationFailed(e: FirebaseException) {
                    Log.w("OTP_FIREBASE", "onVerificationFailed", e)
                }

                override fun onCodeSent(
                    verificationId: String,
                    token: PhoneAuthProvider.ForceResendingToken
                ) {
                    mVerificationId = verificationId
                    Log.d("OTP_FIREBASE", "onCodeSent:$verificationId")
                }
            })
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }

    private fun confirmOTPFirebase(otp: String) {
        try {
            val credential = PhoneAuthProvider.getCredential(mVerificationId, otp)
            auth.signInWithCredential(credential)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                    }
                }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onBackPressed() {
        if (backPressedTime + DOUBLE_PRESS_INTERVAL * 2 > System.currentTimeMillis()) {
            super.onBackPressed()
            return
        } else {
            Toast.makeText(this, getString(R.string.str_back_press), Toast.LENGTH_SHORT).show()
        }
        backPressedTime = System.currentTimeMillis()
    }
}
