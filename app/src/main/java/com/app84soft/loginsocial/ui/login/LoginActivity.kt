package com.app84soft.loginsocial.ui.login

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.app84soft.loginsocial.R
import com.app84soft.loginsocial.base.BaseActivity
import com.app84soft.loginsocial.databinding.ActivityLoginBinding
import com.app84soft.loginsocial.model.request.LoginRequest
import com.app84soft.loginsocial.ui.main.MainActivity
import com.app84soft.loginsocial.utils.*
import com.app84soft.loginsocial.view_model.ViewModelFactory
import com.facebook.CallbackManager
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task

class LoginActivity : BaseActivity<LoginViewModel, ActivityLoginBinding>() {

    private val RC_SIGN_IN = 1000
    private var backPressedTime: Long = 0
    private var isFromWelcome = false
    private var isUserValidated = false
    private var isPassValidated = false

    private lateinit var fbLoginManager: LoginManager
    private lateinit var callbackManager: CallbackManager

    private var mGoogleSignInClient: GoogleSignInClient? = null

    override fun getContentLayout(): Int {
        return R.layout.activity_login
    }

    override fun initViewModel() {
        viewModel =
            ViewModelProvider(this, ViewModelFactory(this))[LoginViewModel::class.java]
    }

    override fun initView() {
        setColorForStatusBar(R.color.colorWhite)
        setLightIconStatusBar(false)

        isFromWelcome = intent.getBooleanExtra(EXTRA_FROM_WELCOME, false)
        if (isFromWelcome) {
            binding.ivBack.visibility = View.VISIBLE
            binding.ivBack.isEnabled = true
        } else {
            binding.ivBack.visibility = View.INVISIBLE
            binding.ivBack.isEnabled = false
        }
    }

    override fun initListener() {
        binding.ivBack.setOnClickListener {
            finish()
        }


        binding.tvForgot.setOnClickListener {
            if (!isDoubleClick()) {

            }
        }


        binding.tvLogin.setOnClickListener {
            if (!isDoubleClick()) {
                when (EMPTY_STRING) {
                    binding.edtUsername.text.toString().trim() -> {
                        Toast.makeText(this, getString(R.string.empty_user), Toast.LENGTH_SHORT).show()
                    }
                    binding.edtPassword.text.toString().trim() -> {
                        Toast.makeText(this, getString(R.string.empty_pass), Toast.LENGTH_SHORT).show()
                    }
                    else -> {
                        viewModel.loginManual(
                            LoginRequest(
                                binding.edtUsername.text.toString().trim(),
                                CommonUtils.encrypt(binding.edtPassword.text.toString().trim()),
                                CommonUtils.getID(this)
                            ),binding.edtPassword.text.toString().trim()
                        )
                    }
                }

            }
        }

    }

    override fun observerLiveData() {
        viewModel.apply {
            loginResponse.observe(this@LoginActivity) {
                startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                finishAffinity()
            }
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            Log.d(
                "TypeLoginActivity",
                "handleSignInResult: " + GsonUtils.getInstance().toJson(account)
            )
        } catch (e: ApiException) {
            Log.e("TypeLoginActivity", "signInResult: " + e.message)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i("TypeLoginActivity", "onActivityResult: CallBack")
        callbackManager.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    override fun onBackPressed() {
        if (isFromWelcome) {
            super.onBackPressed()
        } else {
            if (backPressedTime + DOUBLE_PRESS_INTERVAL * 2 > System.currentTimeMillis()) {
                super.onBackPressed()
                return
            } else {
                Toast.makeText(this, getString(R.string.str_back_press), Toast.LENGTH_SHORT).show()
            }
            backPressedTime = System.currentTimeMillis()
        }
    }

}
