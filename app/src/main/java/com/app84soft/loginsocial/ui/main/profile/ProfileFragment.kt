package com.app84soft.loginsocial.ui.main.profile

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import com.app84soft.loginsocial.R
import com.app84soft.loginsocial.base.AccountUtil
import com.app84soft.loginsocial.base.BaseFragment
import com.app84soft.loginsocial.databinding.FragmentProfileBinding
import com.app84soft.loginsocial.model.request.LogoutRequest
import com.app84soft.loginsocial.ui.login.LoginActivity
import com.app84soft.loginsocial.utils.CommonUtils
import com.app84soft.loginsocial.view_model.ViewModelFactory

class ProfileFragment :
    BaseFragment<ProfileViewModel, FragmentProfileBinding>() {

    companion object {
        fun newInstance(): ProfileFragment {
            return ProfileFragment()
        }
    }

    override fun getContentLayout(): Int {
        return R.layout.fragment_profile
    }

    override fun initViewModel() {
        viewModel =
            ViewModelProvider(
                this,
                ViewModelFactory(binding.root.context)
            )[ProfileViewModel::class.java]
    }

    override fun initView() {
        paddingStatusBar(binding.layoutParent)
    }

    override fun initListener() {
        viewModel.getProfile()
        binding.btnLogout.setOnClickListener {
            if (!isDoubleClick()) {
                val alertDialogBuilder = AlertDialog.Builder(binding.root.context)
                alertDialogBuilder.setIcon(R.drawable.app_logo)
                alertDialogBuilder.setTitle(getString(R.string.app_name))
                alertDialogBuilder.setMessage(getString(R.string.str_log_out))
                alertDialogBuilder.setPositiveButton(getString(R.string.str_yes)) { arg0: DialogInterface, _: Int ->
                    arg0.dismiss()
                    viewModel.logout(LogoutRequest(CommonUtils.getID(binding.root.context)))
                }

                alertDialogBuilder.setNegativeButton(
                    getString(R.string.str_no)
                ) { dialog: DialogInterface, _: Int -> dialog.dismiss() }

                val alertDialog = alertDialogBuilder.create()
                alertDialog.show()
            }
        }

        binding.layoutProfile.setOnClickListener{

        }
        binding.layoutBeLate.setOnClickListener {

        }
        binding.layoutLeaveOfAbsence.setOnClickListener {

        }
        binding.layoutChangePass.setOnClickListener {

        }
        binding.layoutRegulation.setOnClickListener {

        }

    }

    override fun observerLiveData() {
        viewModel.apply {
            logoutResponse.observe(this@ProfileFragment) {
                AccountUtil().getInstance(binding.root.context)?.logout(binding.root.context)
                startActivity(Intent(binding.root.context, LoginActivity::class.java))
            }

            getProfileResponse.observe(this@ProfileFragment){
                binding.tvName.text = it.name
                binding.tvPhone.text = it.phone
            }
        }
    }
}