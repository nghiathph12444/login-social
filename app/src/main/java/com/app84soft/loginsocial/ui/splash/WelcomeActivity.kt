package com.app84soft.loginsocial.ui.splash

import android.content.Intent
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.app84soft.loginsocial.R
import com.app84soft.loginsocial.base.BaseActivity
import com.app84soft.loginsocial.databinding.ActivityWelcomeBinding
import com.app84soft.loginsocial.ui.login.LoginActivity
import com.app84soft.loginsocial.utils.EXTRA_FROM_WELCOME
import com.app84soft.loginsocial.view_model.ViewModelFactory

class WelcomeActivity : BaseActivity<SplashViewModel, ActivityWelcomeBinding>() {

    private var backPressedTime: Long = 0

    override fun getContentLayout(): Int {
        return R.layout.activity_welcome
    }

    override fun initViewModel() {
        viewModel =
            ViewModelProvider(this, ViewModelFactory(this))[SplashViewModel::class.java]
    }

    override fun initView() {
        setColorForStatusBar(R.color.colorPrimary)
    }

    override fun initListener() {
        binding.tvLogin.setOnClickListener {
            if (!isDoubleClick()) {
                val intent = Intent(this, LoginActivity::class.java)
                intent.putExtra(EXTRA_FROM_WELCOME, true)
                startActivity(intent)
            }
        }
        binding.tvRegister.setOnClickListener {
            if (!isDoubleClick()) {

            }
        }
    }

    override fun observerLiveData() {
        viewModel.apply {

        }
    }

    override fun onBackPressed() {
        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            super.onBackPressed()
            return
        } else {
            Toast.makeText(this, getString(R.string.str_back_press), Toast.LENGTH_SHORT).show()
        }
        backPressedTime = System.currentTimeMillis()
    }
}
