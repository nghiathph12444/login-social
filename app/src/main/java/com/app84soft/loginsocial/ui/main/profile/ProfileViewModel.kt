package com.app84soft.loginsocial.ui.main.profile

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import com.app84soft.loginsocial.data.DataManager
import com.app84soft.loginsocial.model.request.LogoutRequest
import com.app84soft.loginsocial.model.response.UserProfile
import com.app84soft.loginsocial.network.Api
import com.app84soft.loginsocial.view_model.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@SuppressLint("CheckResult")
class ProfileViewModel : BaseViewModel() {
    @Inject
    lateinit var dataManager: DataManager
    @Inject
    lateinit var api: Api

    var logoutResponse: MutableLiveData<Boolean> = MutableLiveData()
    fun logout(request: LogoutRequest) {
        api.logout(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrievePostListStart() }
            .doOnTerminate { onRetrievePostListFinish() }
            .subscribe(
                {
                    logoutResponse.postValue(true)
                },
                {
                    handleApiError(it)
                }
            )
    }

    var getProfileResponse: MutableLiveData<UserProfile> = MutableLiveData()
    fun getProfile(){
        api.getProfile()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrievePostListStart() }
            .doOnTerminate { onRetrievePostListFinish() }
            .subscribe(
                {
                    getProfileResponse.postValue(it?.data ?: UserProfile())
                },
                {
                    handleApiError(it)
                }
            )
    }

}