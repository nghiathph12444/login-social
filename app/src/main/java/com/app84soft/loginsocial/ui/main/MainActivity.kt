package com.app84soft.loginsocial.ui.main

import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.app84soft.loginsocial.R
import com.app84soft.loginsocial.base.AccountUtil
import com.app84soft.loginsocial.base.BaseActivity
import com.app84soft.loginsocial.base.PagerFragmentAdapter
import com.app84soft.loginsocial.databinding.ActivityMainBinding
import com.app84soft.loginsocial.ui.main.home.HomeFragment
import com.app84soft.loginsocial.ui.main.profile.ProfileFragment
import com.app84soft.loginsocial.view_model.ViewModelFactory
import com.google.android.material.navigation.NavigationBarView


class MainActivity : BaseActivity<MainViewModel, ActivityMainBinding>(),
    NavigationBarView.OnItemSelectedListener {

    private var backPressedTime: Long = 0
    private lateinit var mPagerAdapter: PagerFragmentAdapter
    private val homeFragment = HomeFragment.newInstance()
    private val profileFragment = ProfileFragment.newInstance()

    companion object {
        const val INDEX_HOME = 0
        const val INDEX_PROFILE = 1
    }

    override fun getContentLayout(): Int {
        return R.layout.activity_main
    }

    override fun initViewModel() {
        viewModel =
            ViewModelProvider(this, ViewModelFactory(this))[MainViewModel::class.java]
    }

    override fun initView() {
        transparentStatusbar()
        setLightIconStatusBar(false)
        initFragmentPager()
    }

    override fun initListener() {
        Log.e(
            "accessToken",
            "accessToken: " + AccountUtil().getInstance(this)?.getAccessToken(this)
        )
    }

    override fun observerLiveData() {
    }

    private fun initFragmentPager() {
        binding.navigation.setOnItemSelectedListener(this)

        mPagerAdapter = PagerFragmentAdapter(this)
        mPagerAdapter.addFragment(homeFragment)
        mPagerAdapter.addFragment(profileFragment)

        binding.viewPager.offscreenPageLimit = mPagerAdapter.itemCount
        binding.viewPager.adapter = mPagerAdapter
        binding.viewPager.isUserInputEnabled = false // disable swiping

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navigation_home -> {
                binding.viewPager.currentItem = INDEX_HOME
                return true
            }
            R.id.navigation_profile -> {
                binding.viewPager.currentItem = INDEX_PROFILE
                return true
            }

        }
        return false
    }

    override fun onBackPressed() {
        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            super.onBackPressed()
            return
        } else {
            Toast.makeText(this, getString(R.string.str_back_press), Toast.LENGTH_SHORT).show()
        }
        backPressedTime = System.currentTimeMillis()
    }
}
