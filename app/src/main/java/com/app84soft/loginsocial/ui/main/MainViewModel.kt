package com.app84soft.loginsocial.ui.main

import android.annotation.SuppressLint
import com.app84soft.loginsocial.data.DataManager
import com.app84soft.loginsocial.network.Api
import com.app84soft.loginsocial.view_model.BaseViewModel
import javax.inject.Inject

@SuppressLint("CheckResult")
class MainViewModel(): BaseViewModel() {
    @Inject
    lateinit var dataManager: DataManager
    @Inject
    lateinit var api: Api

}