package com.app84soft.loginsocial.ui.main.home

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import com.app84soft.loginsocial.data.DataManager
import com.app84soft.loginsocial.model.response.Summary
import com.app84soft.loginsocial.network.Api
import com.app84soft.loginsocial.view_model.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@SuppressLint("CheckResult")
class HomeFragmentViewModel : BaseViewModel() {
    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var api: Api

    var summaryResponse: MutableLiveData<Summary> = MutableLiveData()
    fun getSummary(){
        api.getSummary()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {onRetrievePostListStart()}
            .doOnTerminate { onRetrievePostListFinish()}
            .subscribe(
                {
                    summaryResponse.postValue(it?.data as Summary)
                },
                {
                    handleApiError(it)
                }
            )

    }
}