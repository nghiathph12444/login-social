package com.app84soft.loginsocial.ui.splash

import android.content.Intent
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.ViewModelProvider
import com.app84soft.loginsocial.R
import com.app84soft.loginsocial.base.BaseActivity
import com.app84soft.loginsocial.databinding.ActivitySplashBinding
import com.app84soft.loginsocial.model.constants.TypeAutoLogin.*
import com.app84soft.loginsocial.ui.login.LoginActivity
import com.app84soft.loginsocial.ui.main.MainActivity
import com.app84soft.loginsocial.utils.CommonUtils.printKeyHash
import com.app84soft.loginsocial.utils.DOUBLE_PRESS_INTERVAL
import com.app84soft.loginsocial.view_model.ViewModelFactory

class SplashActivity : BaseActivity<SplashViewModel, ActivitySplashBinding>() {

    override fun getContentLayout(): Int {
        return R.layout.activity_splash
    }

    override fun initViewModel() {
        viewModel =
            ViewModelProvider(this, ViewModelFactory(this))[SplashViewModel::class.java]
    }

    override fun initView() {
        setColorForStatusBar(R.color.colorWhite)
    }

    override fun initListener() {
        printKeyHash(this)

        Handler(Looper.getMainLooper()).postDelayed(
            { viewModel.checkLogin() },
            DOUBLE_PRESS_INTERVAL
        )
    }

    override fun observerLiveData() {
        viewModel.apply {
            autoLoginResponse.observe(this@SplashActivity) {
                when (it) {
                    TO_MAIN -> {
                        startActivity(Intent(this@SplashActivity, MainActivity::class.java))
                    }
                    else -> {
                        startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
                    }
                }
                finishAffinity()
            }
        }
    }
}
