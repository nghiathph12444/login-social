package com.app84soft.loginsocial.ui.login

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.app84soft.loginsocial.base.AccountUtil
import com.app84soft.loginsocial.model.constants.TypeLogin
import com.app84soft.loginsocial.model.request.LoginRequest
import com.app84soft.loginsocial.network.Api
import com.app84soft.loginsocial.view_model.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


@SuppressLint("CheckResult")
class LoginViewModel(val context: Context) : BaseViewModel() {
    @Inject
    lateinit var api: Api

    var loginResponse: MutableLiveData<Boolean> = MutableLiveData()
    fun loginManual(request: LoginRequest, password:String) {
        api.loginManual(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrievePostListStart() }
            .doOnTerminate { onRetrievePostListFinish() }
            .subscribe(
                {
                    AccountUtil().getInstance(context)?.saveLoginInfo(it?.data, request.username, password, TypeLogin.MANUAL)
                    AccountUtil().saveUserInfo(it?.data)
                    loginResponse.postValue( true)
                },
                {
                    handleApiError(it)
                }
            )
    }
}