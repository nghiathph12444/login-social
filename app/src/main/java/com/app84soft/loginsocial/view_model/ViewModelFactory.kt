package com.app84soft.loginsocial.view_model

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app84soft.loginsocial.ui.login.LoginViewModel
import com.app84soft.loginsocial.ui.main.MainViewModel
import com.app84soft.loginsocial.ui.main.home.HomeFragmentViewModel
import com.app84soft.loginsocial.ui.main.profile.ProfileViewModel
import com.app84soft.loginsocial.ui.otp.OtpViewModel
import com.app84soft.loginsocial.ui.splash.SplashViewModel

class ViewModelFactory(private val context: Context) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(context) as T
        }
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel() as T
        }
        if (modelClass.isAssignableFrom(HomeFragmentViewModel::class.java)) {
            return HomeFragmentViewModel() as T
        }
        if (modelClass.isAssignableFrom(ProfileViewModel::class.java)) {
            return ProfileViewModel() as T
        }
        if (modelClass.isAssignableFrom(SplashViewModel::class.java)) {
            return SplashViewModel(context) as T
        }
        if (modelClass.isAssignableFrom(OtpViewModel::class.java)){
            return OtpViewModel(context) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")

    }
}