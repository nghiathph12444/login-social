package com.app84soft.loginsocial.view_model

import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app84soft.loginsocial.R
import com.app84soft.loginsocial.injection.DaggerViewModelInjector
import com.app84soft.loginsocial.injection.NetworkModule
import com.app84soft.loginsocial.injection.ViewModelInjector
import com.app84soft.loginsocial.model.response.Message
import com.app84soft.loginsocial.ui.login.LoginViewModel
import com.app84soft.loginsocial.ui.main.MainViewModel
import com.app84soft.loginsocial.ui.main.home.HomeFragmentViewModel
import com.app84soft.loginsocial.ui.main.profile.ProfileViewModel
import com.app84soft.loginsocial.ui.otp.OtpViewModel
import com.app84soft.loginsocial.ui.splash.SplashViewModel
import com.app84soft.loginsocial.utils.GsonUtils
import com.app84soft.loginsocial.utils.myapp
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.HttpException
import java.io.File
import java.io.IOException
import java.net.HttpURLConnection
import java.net.SocketTimeoutException
import javax.net.ssl.HttpsURLConnection

abstract class BaseViewModel : ViewModel() {

    val isLoading: MutableLiveData<Boolean> = MutableLiveData()
    val errorMessage: MutableLiveData<Int?> = MutableLiveData()
    val responseMessage: MutableLiveData<String?> = MutableLiveData()

    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .applicationComponent(myapp!!)
        .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is LoginViewModel -> injector.inject(this)
            is MainViewModel -> injector.inject(this)
            is HomeFragmentViewModel -> injector.inject(this)
            is ProfileViewModel -> injector.inject(this)
            is SplashViewModel -> injector.inject(this)
            is OtpViewModel -> injector.inject(this)


        }
    }

    protected fun onRetrievePostListStart() {
        isLoading.postValue(true)
        errorMessage.postValue(null)
    }

    protected fun onRetrievePostListFinish() {
        Handler(Looper.getMainLooper()).postDelayed({
            isLoading.postValue(false)
        }, 500)
    }

    protected fun handleApiError(error: Throwable?) {
        if (error == null) {
            errorMessage.postValue(R.string.api_default_error)
            return
        }

        if (error is HttpException) {
            Log.w("ERROR", error.message() + error.code())
            when (error.code()) {
                HttpURLConnection.HTTP_BAD_REQUEST -> try {
                    val message: Message = GsonUtils.deserialize(
                        error.response()?.errorBody()?.string(),
                        Message::class.java
                    )
                    responseMessage.postValue(message.message)
                } catch (e: IOException) {
                    e.printStackTrace()
                    responseMessage.postValue(error.message)
                }
                HttpsURLConnection.HTTP_UNAUTHORIZED -> errorMessage.postValue(R.string.str_authe)
                HttpsURLConnection.HTTP_FORBIDDEN, HttpsURLConnection.HTTP_INTERNAL_ERROR, HttpsURLConnection.HTTP_NOT_FOUND -> responseMessage.postValue(
                    error.message
                )
                else -> responseMessage.postValue(error.message)
            }
        } else if (error is SocketTimeoutException) {
            errorMessage.postValue(R.string.text_all_has_error_timeout)
        } else if (error is IOException) {
            errorMessage.postValue(R.string.text_all_has_error_network)
        } else {
            if (!TextUtils.isEmpty(error.message)) {
                responseMessage.postValue(error.message)
            } else {
                errorMessage.postValue(R.string.text_all_has_error_please_try)
            }
        }
    }

    fun toMultipartBody(name: String, file: File): MultipartBody.Part {
        val reqFile = file.asRequestBody("image/*".toMediaTypeOrNull())
        return MultipartBody.Part.createFormData(name, file.name, reqFile)
    }

    fun toMultipartBody1(name: String, file: File): MultipartBody.Part {
        val reqFile = file.asRequestBody("video/*, image/*".toMediaTypeOrNull())
        return MultipartBody.Part.createFormData(name, file.name, reqFile)
    }

}