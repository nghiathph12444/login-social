package com.app84soft.loginsocial.base

import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import com.app84soft.loginsocial.model.constants.TypeInputText
import com.google.android.material.textfield.TextInputLayout

class CustomTextWatcher(val textInputLayout: TextInputLayout, val type: TypeInputText) :
    TextWatcher {

    var validated = false

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(s: Editable?) {
        if (TextUtils.isEmpty(s.toString().trim())) {
            validated = false
        } else {

        }
    }

}