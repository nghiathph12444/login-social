package com.app84soft.loginsocial.base

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.app84soft.loginsocial.BuildConfig
import com.app84soft.loginsocial.R
import com.app84soft.loginsocial.injection.NetworkModule
import com.app84soft.loginsocial.model.constants.TypeLogin
import com.app84soft.loginsocial.model.response.User
import com.app84soft.loginsocial.utils.*
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.gson.Gson

class AccountUtil {
    private var accountUtil: AccountUtil? = null
    private var pref: SharedPreferences? = null
    private var editor: SharedPreferences.Editor? = null

    fun getInstance(context: Context): AccountUtil? {
        if (accountUtil == null) accountUtil = AccountUtil()
        if (pref == null) {
            pref = PreferenceManager.getDefaultSharedPreferences(context)
            accountUtil?.pref = pref
        }
        return accountUtil
    }

    fun saveLoginInfo(
        user: User?,
        email: String?,
        password: String?,
        typeLogin: TypeLogin
    ) {
        editor = pref?.edit()
        editor?.putString(PREF_USER_DATA, Gson().toJson(user))
        editor?.putString(PREF_ACCOUNT, email)
        editor?.putString(PREF_PASSWORD, password)
        editor?.putInt(PREF_LOGIN_TYPE, typeLogin.value)
        editor?.putString(PREF_ACCESS_TOKEN, user?.accessToken ?: EMPTY_STRING)
        editor?.apply()

        NetworkModule.mToken = user?.accessToken.toString()
    }

    fun saveUserInfo(user: User?) {
        editor = pref?.edit()
        editor?.putString(PREF_USER_DATA, Gson().toJson(user))
        editor?.apply()
    }

    fun getUserInfo(): User {
        var user = Gson().fromJson(
            pref?.getString(PREF_USER_DATA, EMPTY_STRING),
            User::class.java
        )
        if (user == null) user = User()
        return user
    }

    fun getTypeLogin(): Int? {
        return pref?.getInt(PREF_LOGIN_TYPE, TypeLogin.NOT_LOGIN.value)
    }

    fun getAccount(): String? {
        return pref?.getString(PREF_ACCOUNT, EMPTY_STRING)
    }

    fun getPassword(): String? {
        return pref?.getString(PREF_PASSWORD, EMPTY_STRING)
    }

    fun getAccessToken(context: Context): String {
        return if (BuildConfig.DEBUG) {
            val user = Gson().fromJson(
                pref?.getString(PREF_USER_DATA, EMPTY_STRING),
                User::class.java
            )
            user?.accessToken ?: context.getString(R.string.text_all_has_error_timeout)
        } else {
            context.getString(R.string.text_all_has_error_timeout)
        }
    }

    fun logout(context: Context) {
        editor = pref?.edit()
        editor?.remove(PREF_USER_DATA)
        editor?.remove(PREF_ACCOUNT)
        editor?.remove(PREF_PASSWORD)
        editor?.putInt(PREF_LOGIN_TYPE, TypeLogin.NOT_LOGIN.value)
        editor?.remove(PREF_ACCESS_TOKEN)
        editor?.apply()

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(context.getString(R.string.server_client_id))
            .requestEmail()
            .build()
        val mGoogleApiClient = GoogleSignIn.getClient(context, gso)
        mGoogleApiClient.signOut()

        NetworkModule.mToken = EMPTY_STRING
    }
}