package com.app84soft.loginsocial.base

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class PagerFragmentAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
    private var fragmentList: ArrayList<Fragment> = ArrayList()
    private var fragmentTitleList: ArrayList<String> = ArrayList()

    fun addFragment(fragment: Fragment) {
        fragmentList.add(fragment)
        fragmentTitleList.add(fragment.javaClass.simpleName)
    }

    fun addFragment(fragment: Fragment?, title: String?) {
        fragment?.let { fragmentList.add(it) }
        title?.let { fragmentTitleList.add(it) }
    }

//    override fun getPageTitle(position: Int): String {
//        return fragmentTitleList[position]
//    }

    override fun getItemCount(): Int {
        return fragmentList.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragmentList[position]
    }


}