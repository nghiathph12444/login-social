package com.app84soft.loginsocial.base

import android.content.Context
import com.google.android.material.textfield.TextInputLayout
import android.util.AttributeSet

class CustomTextInputLayout : TextInputLayout {
    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(
        context!!, attrs
    ) {
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context!!, attrs, defStyleAttr
    ) {}

    override fun drawableStateChanged() {
        super.drawableStateChanged()
        clearEditTextColorFilter()
    }

    override fun setError(error: CharSequence?) {
        super.setError(error)
        clearEditTextColorFilter()
    }

    private fun clearEditTextColorFilter() {
        val editText = editText
        if (editText != null) {
            val background = editText.background
            background?.clearColorFilter()
        }
    }
}