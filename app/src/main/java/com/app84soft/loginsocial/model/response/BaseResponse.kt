package com.app84soft.loginsocial.model.response


data class BaseResponse <T> (
        val data: T?,
        val message: String
)
data class BaseArrayResponse <T> (
        val data: List<T>?,
        val message: String
)
data class BaseResponseNoBody (
        val message: String?
)