package com.app84soft.loginsocial.model.response


import com.google.gson.annotations.SerializedName

data class Summary(
    @SerializedName("numLate")
    var numLate: Int? = 0,
    @SerializedName("numOff")
    var numOff: Int? = 0,
    @SerializedName("totalPunishFee")
    var totalPunishFee: Double? = 0.0
)