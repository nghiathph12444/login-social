package com.app84soft.loginsocial.model.constants

enum class TypeAutoLogin(val value: Int) {
    TO_WELCOME(0),
    TO_LOGIN(1),
    TO_OTP(2),
    TO_MAIN(3)
}