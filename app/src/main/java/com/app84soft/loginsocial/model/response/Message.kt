package com.app84soft.loginsocial.model.response

import com.google.gson.annotations.SerializedName

data class Message(
    @SerializedName("message") val message: String
)