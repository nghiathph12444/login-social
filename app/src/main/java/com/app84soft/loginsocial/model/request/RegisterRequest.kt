package com.app84soft.loginsocial.model.request
import com.google.gson.annotations.SerializedName

data class RegisterRequest(
    @SerializedName("name") val name: String?,
    @SerializedName("phone") val phone: String?,
    @SerializedName("password") val password: String?
)