package com.app84soft.loginsocial.model.request

import com.google.gson.annotations.SerializedName

data class LogoutRequest(
    @SerializedName("deviceKey") val deviceKey: String?
)