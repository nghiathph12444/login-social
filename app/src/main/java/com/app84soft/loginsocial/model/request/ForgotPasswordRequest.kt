package com.app84soft.loginsocial.model.request

import com.google.gson.annotations.SerializedName

data class ForgotPasswordRequest(
    @SerializedName("deviceKey") val deviceKey: String?,
    @SerializedName("account") val account: String?,
    @SerializedName("password") val password: String?,
    @SerializedName("code") val code: String?
)