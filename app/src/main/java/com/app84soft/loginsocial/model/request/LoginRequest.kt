package com.app84soft.loginsocial.model.request

import com.google.gson.annotations.SerializedName

data class LoginRequest(
    @SerializedName("username") val username: String?,
    @SerializedName("passwordRequest") val passwordRequest: String?,
    @SerializedName("deviceKey") val deviceKey: String?

)