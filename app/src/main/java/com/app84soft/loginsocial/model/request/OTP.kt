package com.app84soft.loginsocial.model.request
import com.google.gson.annotations.SerializedName
data class OTP (
    @SerializedName("deviceKey") val deviceKey : String?,
    @SerializedName("account") val account : String?
)