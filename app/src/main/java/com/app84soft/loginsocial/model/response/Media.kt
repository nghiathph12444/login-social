package com.app84soft.loginsocial.model.response


import com.google.gson.annotations.SerializedName

open class Media {
    @SerializedName("createdAt")
    var createdAt: String?= null

    @SerializedName("deleted")
    var deleted: Boolean?= null

    @SerializedName("duration")
    var duration: Int?= null

    @SerializedName("height")
    var height: Int?= null

    @SerializedName("id")
    var id: Int?= null

    @SerializedName("originName")
    var originName: String?= null

    @SerializedName("originUrl")
    var originUrl: String?= null

    @SerializedName("size")
    var size: Int?= null

    @SerializedName("thumbUrl")
    var thumbUrl: String?= null

    @SerializedName("type")
    var type: Int?= null

    @SerializedName("updatedAt")
    var updatedAt: String?= null

    @SerializedName("width")
    var width: Int? = null
}
