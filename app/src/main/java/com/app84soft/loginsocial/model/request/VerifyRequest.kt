package com.app84soft.loginsocial.model.request

import com.google.gson.annotations.SerializedName

data class VerifyRequest(
    @SerializedName("deviceId") val deviceId: String?,
    @SerializedName("account") val account: String?,
    @SerializedName("code") val code: String?
)