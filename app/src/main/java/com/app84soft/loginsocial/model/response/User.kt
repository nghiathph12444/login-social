package com.app84soft.loginsocial.model.response


import com.google.gson.annotations.SerializedName

open class User {
    @SerializedName("accessToken")
    var accessToken: String? = null

    @SerializedName("avatar")
    var avatar: Media? = null

    @SerializedName("avatarId")
    var avatarId: Int? = null

    @SerializedName("createdAt")
    var createdAt: String? = null

    @SerializedName("deleted")
    var deleted: Boolean? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("follower")
    var follower: Int? = null

    @SerializedName("following")
    var following: Int? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("password")
    var password: String? = null

    @SerializedName("phone")
    var phone: String? = null

    @SerializedName("role")
    var role: Int? = null

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("totalAmount")
    var totalAmount: Int? = null

    @SerializedName("updatedAt")
    var updatedAt: String? = null

    @SerializedName("verifiedEmail")
    var verifiedEmail: Boolean? = null

    @SerializedName("verifiedPhone")
    var verifiedPhone: Boolean? = null

    @SerializedName("address")
    var address: String? = ""

    @SerializedName("birthday")
    var birthday: String? = ""

    @SerializedName("positionInCompany")
    var positionInCompany: String? = ""

    @SerializedName("department")
    var department: String? = ""
}