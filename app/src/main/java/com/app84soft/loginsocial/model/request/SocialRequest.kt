package com.app84soft.loginsocial.model.request


import com.google.gson.annotations.SerializedName

data class SocialRequest(
    @SerializedName("accessToken")
    val accessToken: String
)