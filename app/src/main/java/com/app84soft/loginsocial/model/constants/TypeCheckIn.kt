package com.app84soft.loginsocial.model.constants

enum class TypeCheckIn(val value: Int) {
    TYPE_LATE(1),
    TYPE_OFF(0)
}