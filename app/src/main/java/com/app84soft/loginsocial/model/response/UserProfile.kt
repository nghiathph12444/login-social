package com.app84soft.loginsocial.model.response


import com.google.gson.annotations.SerializedName

data class UserProfile(
    @SerializedName("accessToken")
    var accessToken: Any? = Any(),
    @SerializedName("address")
    var address: Any? = Any(),
    @SerializedName("avatar")
    var avatar: Any? = Any(),
    @SerializedName("avatarId")
    var avatarId: Any? = Any(),
    @SerializedName("birthday")
    var birthday: Any? = Any(),
    @SerializedName("createdAt")
    var createdAt: String? = "",
    @SerializedName("deleted")
    var deleted: Boolean? = false,
    @SerializedName("email")
    var email: String? = "",
    @SerializedName("gender")
    var gender: Any? = Any(),
    @SerializedName("id")
    var id: Int? = 0,
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("passwordRequest")
    var passwordRequest: Any? = Any(),
    @SerializedName("phone")
    var phone: String? = "",
    @SerializedName("role")
    var role: Int? = 0,
    @SerializedName("status")
    var status: Int? = 0,
    @SerializedName("updatedAt")
    var updatedAt: String? = "",
    @SerializedName("username")
    var username: Any? = Any()
)