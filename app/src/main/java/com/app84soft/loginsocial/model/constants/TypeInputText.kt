package com.app84soft.loginsocial.model.constants

enum class TypeInputText(val value: Int) {
    DEFAULT(0),
    PHONE(1),
    PASSWORD(2),
    CONFIRM_PASS(3),
    OTP(3)
}