package com.app84soft.loginsocial.model.constants

enum class TypeLogin(val value: Int) {
    NOT_LOGIN(0),
    MANUAL(1),
    GOOGLE(2),
    FACEBOOK(3)
}