package com.app84soft.loginsocial.injection

import com.app84soft.loginsocial.ui.login.LoginViewModel
import com.app84soft.loginsocial.ui.main.MainViewModel
import com.app84soft.loginsocial.ui.main.home.HomeFragmentViewModel
import com.app84soft.loginsocial.ui.main.profile.ProfileViewModel
import com.app84soft.loginsocial.ui.otp.OtpViewModel
import com.app84soft.loginsocial.ui.splash.SplashViewModel
import dagger.Component

/**
 * Component providing inject() methods for presenters.
 */
@Component(modules = [(NetworkModule::class)], dependencies = [ApplicationComponent::class])
@ViewModelScope
interface ViewModelInjector {
    /**
     * Injects required dependencies into the specified PostListViewModel.
     * @param postListViewModel PostListViewModel in which to inject the dependencies
     */
    fun inject(viewModel: LoginViewModel)
    fun inject(viewModel: MainViewModel)
    fun inject(viewModel: HomeFragmentViewModel)
    fun inject(viewModel: ProfileViewModel)
    fun inject(viewModel: SplashViewModel)
    fun inject(viewModel: OtpViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector
        fun networkModule(networkModule: NetworkModule): Builder
        fun applicationComponent(applicationComponent: ApplicationComponent): Builder
    }
}