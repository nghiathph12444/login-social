package com.app84soft.loginsocial.injection

import android.content.Context
import androidx.preference.PreferenceManager
import com.app84soft.loginsocial.BuildConfig
import com.app84soft.loginsocial.network.Api
import com.app84soft.loginsocial.utils.CommonUtils
import com.app84soft.loginsocial.utils.DEFAULT_LANGUAGE
import com.app84soft.loginsocial.utils.EMPTY_STRING
import com.app84soft.loginsocial.utils.PREF_ACCESS_TOKEN
import com.app84soft.loginsocial.utils.extension.getCurrentDayUTC
import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Module which provides all required dependencies about network
 */
@Module
// Safe here as we are dealing with a Dagger 2 module
@Suppress("unused")
object NetworkModule {

    var mToken = ""
    private const val TIME_OUT: Long = 10
    private var mLanguage = DEFAULT_LANGUAGE

    /**
     * Provides the Post service implementation.
     * @param retrofit the Retrofit object used to instantiate the service
     * @return the Post service implementation.
     */
    @Provides
    @Reusable
    @JvmStatic
    internal fun providePostApi(retrofit: Retrofit): Api {
        return retrofit.create(Api::class.java)
    }

    /**
     * Provides the Retrofit object.
     * @return the Retrofit object
     */
    @Provides
    @Reusable
    @JvmStatic
    internal fun provideRetrofitInterface(@ApplicationContext context: Context): Retrofit {
        val mPrefs = PreferenceManager.getDefaultSharedPreferences(context)

        if (mToken == EMPTY_STRING) {
            mToken = mPrefs.getString(PREF_ACCESS_TOKEN, EMPTY_STRING) ?: EMPTY_STRING
        }
        val privateKey = CommonUtils.encrypt(getCurrentDayUTC())

        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(TIME_OUT, TimeUnit.SECONDS)
        httpClient.readTimeout(TIME_OUT, TimeUnit.SECONDS)

        httpClient.addInterceptor { chain ->
            val request = chain.request().newBuilder()
                .addHeader("Content-Type", "application/json; charset=utf-8")
                .addHeader("Authorization", "Bearer $mToken")
                .addHeader("Accept-Language", mLanguage)
                .addHeader("Private-Key",privateKey)
                .build()
            chain.proceed(request)
        }

        if (BuildConfig.DEBUG){
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(logging) // <-- this is the important line!
        }

        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .client(httpClient.build())
            .build()
    }
}