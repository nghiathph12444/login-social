package com.app84soft.loginsocial.network

import com.app84soft.loginsocial.model.request.*
import com.app84soft.loginsocial.model.response.*
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface Api {
    @POST("v1/auth/signin")
    fun loginManual(@Body request: LoginRequest): Observable<BaseResponse<User>>

    @POST("v1/auth/login-facebook")
    fun loginFacebook(@Body request: SocialRequest): Observable<BaseResponse<User>>

    @POST("v1/auth/login-google")
    fun loginGoogle(@Body request: SocialRequest): Observable<BaseResponse<User>>

    @POST("v1/auth/signup")
    fun register(@Body request: RegisterRequest): Observable<BaseResponse<User>>

    @POST("v1/auth/verify-phone")
    fun verifyPhone(@Body request: VerifyRequest): Observable<BaseResponseNoBody>

    @POST("v1/auth/change-pass-forgot")
    fun forgotPassword(@Body request: ForgotPasswordRequest): Observable<BaseResponseNoBody>

    @POST("v1/user/logout")
    fun logout(@Body request: LogoutRequest): Observable<BaseResponseNoBody>

    @POST("v1/auth/send-otp")
    fun getOTP(@Body getOTP: OTP): Observable<BaseResponseNoBody>


//    @POST("v1/absence/add-absence")
    @GET("v1/statistic/get-home-summary")
    fun getSummary() : Observable<BaseResponse<Summary>>


    @GET("v1/user/get-my-profile")
    fun getProfile(): Observable<BaseResponse<UserProfile>>
}